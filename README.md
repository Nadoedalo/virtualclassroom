# Virtual Class Room
[comment]: <> (TODO add logo and links to repositories)
[comment]: <> (TODO choose and add the canvas library)
[comment]: <> (TODO add badges for build / test coverage etc)
[comment]: <> (TODO add EMOJIS! And logo images. This should be pretty!)

## Table of contents
- [Introduction](#Introduction)
- [Features](#Features)
- [Installation](#Installation)
- [Production build](#Production build)
- [Development](#Development)
- [Library exports](#Library exports)
- [Project structure](#Project structure)
- [FAQ](#FAQ)
- [Changelog](#Changelog)

## Introduction
This project is just a demo of WebRTC video chat done as a research on the matter.

It implements both front- and back- end done via custom-made BackboneJS-like ES6 library and NodeJS. Bundling is available via [Webpack](https://webpack.js.org/), tests driven by [Jest](https://jestjs.io/)

## Features
- WebSockets chat
- WebRTC video / audio streaming
- Drawing on canvas with different tools
- Synchronising drawings for different users

## Installation
- Install prerequisites:
    - [Ubuntu ^16.04.6 LTS](https://ubuntu.com/tutorials/install-ubuntu-server#1-overview)
    - [NodeJS ^12.18.3](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages)
- Clone the project
- Run `npm install`

## Production build
1. Configure [`/server/config/main.js`](server/config/main.js)
2. Run `npm install`
3. Build the project `npm run release`
4. Run web server `npm run server`
5. Enjoy on [https://localhost](https://localhost) or your.domain.name

After successfully configuring [`/server/config/main.js`](server/config/main.js) just repeat steps 2-5

## Development
1. Configure [`/server/config/main.js`](server/config/main.js)
2. Run
    * `npm run watch` if you want Webpack to react to changes
    * `npm run build` if you want just the static files without live rebuild
3. Run web server in separate tab `npm run devserver`
4. Enjoy your app on [https://localhost](https://localhost)

## Library exports
TODO - update once API finished

## Project structure
TODO - when the demo will be finished this section will be updated

## FAQ
#### Will there be export of just static front-end part?
Sure thing! Export of the files are configurable and are already bundled properly - production export can be configured in [main.js](server/config/main.js). Archive export is still in development, though.
#### Is there hot reload support?
As of moment - no, it is not implemented but is in [TODO](webpack.config.js)
#### How to use the bundle?
Example can be found at the end of [`index.html`](static/index.html) inside of the `script` section
#### Why the library is polluting globalThis?
`libraryTarget: module` will be supported in Webpack 5 which at the moment is still in development. [Issue](https://github.com/webpack/webpack/issues/2933#issuecomment-652942095)

## Changelog
Available [here](CHANGELOG.md)