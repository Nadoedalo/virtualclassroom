/**
 * Nadoedalo aka Ihor Shamrai
 * This is the main file to kickstart the app
 * Handles global events,
 * injects to app with common elements,
 * kickstarting routing,
 * etc etc...
 * */
'use strict';
import html from '~/templates/baseLayout.html';
import mainStyle from '~/../design/styles/main.scss';
import { app } from '~/app.js';
import Router from '~/router.js';
export const Main = function (options) {
	if (!options.el) { throw new Error('"el" property is required'); }
	this.el = options.el; // main element to attach to
	const parser = new DOMParser();
	const parsed = parser.parseFromString(html, 'text/html');
	const nodesArr = parsed.body.childNodes;
	nodesArr.forEach( (node) => {
		this.el.appendChild(node);
	});
	/**
	 * Binding to have proper state management based on links
	 * */
	this.el.addEventListener('click', function (event) { //will listen to all clicks on the element and trigger on click on links

		let target = event.target,
				href = target.getAttribute('href');

		if (target.hostname === window.location.hostname && target.getAttribute('target') !== '_blank' && href && href !== '#') {

			event.preventDefault();
			app.router.navigate(href);
		}
	});
	//Kickstart the app
	app.el = this.el;
	app.router = new Router();
	return this;
};