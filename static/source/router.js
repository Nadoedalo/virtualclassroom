import { Router } from '~/core';
import { app } from '~/app';

export default class AppRouter extends Router {
	initialize () {
		this.routes = {
			'/' : 'main',
			'/room/:id' : 'room',
			default : 'main'
		};
	}
	main() {
		const promises = [
			import('~/modules/Connect')
		];
		window.Promise.all(promises).then((loader) => {
			let nodes;
			const Connect = loader[0];
			const connectModel = new Connect.Model();
			const connectView = new Connect.View({
				model: connectModel
			});
			nodes = app.el.querySelectorAll('.connectView');
			this.replaceNodes(nodes, connectView.el);
		});
		//TODO name yourself
		//TODO copy room link with ID
		//TODO show login-like form
	}
	room(route, id) {
		console.log('room!', route, id);
		const promises = [
			import('~/modules/ConnectedUser'),
			import('~/modules/Chat')
		];
		window.Promise.all(promises).then((loader) => {
			let nodes;
			const ConnectedUser = loader[0];
			const Chat = loader[1];
			/*initializing ConnectedUser module*/
			const connectedUserCollection = new ConnectedUser.Collection([], {
				model : ConnectedUser.Model
			});
			const connectedUserCollectionView = new ConnectedUser.CollectionView({
				collection: connectedUserCollection
			});
			nodes = app.el.querySelectorAll('.connectedUserCollectionView');
			this.replaceNodes(nodes, connectedUserCollectionView.el);

			/*initializing chat*/
			const chatCollection = new Chat.Collection([], {
				model : Chat.Model
			});
			const chat = new Chat.CollectionView({
				collection: chatCollection
			});
			nodes = app.el.querySelectorAll('.chatCollectionView');
			this.replaceNodes(nodes, chat.el);
		});
		//TODO get roomID
		//TODO set mode to either user / master
		//TODO show canvas
		//TODO open websocket channel to sync up canvas drawings && chat messages
		//TODO start recieving and streaming video & audio
	}
}