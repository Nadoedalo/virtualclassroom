'use strict';
export class Abstract {
    /**
     * Events-specific functions
     * */
    constructor () {
        this._listeners = {
            /*
            * event : [[callback, context]]
            * */
        };
        this._listeningTo = { //a subject to refactor. Seems useless.
            /*
            * event : [[callback, otherContext]]
            * */
        };
    }
    trigger (event, ...args) {
        //FIXME check performance. Seems like A LOT of unnecessary actions for reducing to ONE loop.
        const allListeners = this._listeners['all'] || [];
        const listeners = this._listeners[event] || [];
        const merged = [...allListeners, ...listeners];

        for (let i = 0, length = merged.length; i < length; i++) {
            let callback = merged[i][0];
            let context = merged[i][1];
            callback.call(context, ...args, event);
        }
        return this;
    }
    on (event, callback, context) {

        if (!this._listeners[event]) {

            this._listeners[event] = [];
        }
        return this._listeners[event].push([callback, context]);
    }
    off (event, callback) {

        if (this._listeners[event]) {

            if (callback) {
                let arr = this._listeners[event];
                for (let i = 0, length = arr.length; i < length; i++) {
                    let arrCallback = arr[i][0];
                    if (arrCallback === callback) {
                        this._listeners[event] = this._listeners[event].splice(i, 1);
                        break;
                    }
                }
            } else {

                delete this._listeners[event];
            }
        } else {

            this._listeners = {};
        }

        return this;
    }
    once (event, callback, context) {
        /**
         * check for unexpected behavior
         * Especially check for overriding ONCE with ON and same callback reference
         * */
        let proxy = new Proxy(callback, {
            apply : () => {
                this.off(event, proxy);
            }
        });

        return this.on(event, proxy, context);
    }
    //TODO change "other" to something more appropriate
    //Should I use _listeningTo at all?
    //Leaks memory if nobody called stopListening before removing the element from DOM. Is there a solution?
    listenTo (other, event, callback) {

        if(!this._listeningTo[event]) {

            this._listeningTo[event] = new WeakMap();
        }

        other.on(event, callback, this);
        /**
         * potential issue when using static methods as a callback?
         * Since callback signature serves as unique key
         * Thus may produce silent override?
         * Need to research more on how functions behave as unique key for Maps.
         * */
        this._listeningTo[event].set(callback, other);
        return this._listeningTo[event].has(callback);
    }
    stopListening (other, event, callback) {

        if (this._listeningTo[event]) {

            if (callback) {

                this._listeningTo[event].delete(callback);
            } else {

                delete this._listeningTo[event];
            }
        } else {

            this._listeningTo = {};
        }

        if (other) {

            other.off(event, callback); // FIXME this should remove ONLY EVENTS ADDED BY this.listenTo!!!!
        }
        return this;
    }
    listenToOnce (other, event, callback) {
        let proxy = new Proxy(callback, {
            apply : () => {
                this.stopListening(other, event, callback);
            }
        });

        return this.listenTo(other, event, proxy);
    }
}