'use strict';
import { Abstract } from '~/core';
import { template as _template } from 'underscore';
const eventSplitter = /\s(.+)/; //everything after first space is considered a second argument
export class View extends Abstract {
    constructor (options) {
        super();
        options = options || {};
        this.importTemplate = options.importTemplate || null; /*FIXME just a quick fix for a webpack*/
        this._internal = {}; //Used for getters / setters and storing original values. Do not access directly.
        this._setDefaultProperties();
        this.model = options.model || null;
        this.collection = options.collection || null;
        this.initialize(options);
        this.bindEvents();
        return this;
    }
    initialize () {
        return this;
    }
    render () {
        this.template().then((html) => {
            this.appendNodesToThisEl(this.parseHtmlString(html));
        }, console.error.bind(console));
    }
    get tagName () {
        return this._internal.tagName || 'div';
    }
    set tagName (name) {
        this._internal.tagName = name;
        this._createElement(name); //should this be here at all? Just playing with the idea
        this.bindEvents();
        return this.tagName;
    }
    get className () {
        return this._internal.className || '';
    }
    /**
     * @param nameList {String} - split by spaces
     * */
    set className (nameList) {
        let nameArr = nameList.split(' ');
        this._internal.className = nameList;
        nameArr = nameArr.reduce((memo, val) => {
            if (val) {memo.push(val);}
            return memo;
        }, []);
        this.el.className = '';
        this.el.classList.add(...nameArr);
    }
    template (name, data, cache) { //requires name or className set

        return new Promise((resolveTemplate, rejectTemplate) => {
            data = data || (this.model ? Object.assign({}, this.model.attributes) : void 0) || {};
            name = name || this.className || '';
            if(!name){throw new Error('No name specified for template');}
            //TODO add translates support?
            cache = cache === undefined ? true : cache;
            this.getTemplate(name)
                .then((html) => {
                    let template = _template(html);
                    let compiledTemplate = template(data);
                    this.template = function (name, new_data) {
                        return new Promise((resolve) => {
                            if (cache) {
                                resolve(compiledTemplate);
                            } else {
                                resolve(template(new_data));
                            }
                        });
                    };
                    resolveTemplate(compiledTemplate);
                }, rejectTemplate);
        });
    }
    /**
     * @param name {String}
     * @returns {Promise}
     * */
    getTemplate (name) {
        name = name || this.className || '';
        if (!name) {
            throw new Error('No name specified');
        }
        return new Promise((resolve) => {
            if (this.importTemplate) { /*FIXME just a quick fix for a webpack*/
                resolve(this.importTemplate);
                return;
            }
            throw new Error('specify importTemplate');
        });
    }
    _setDefaultProperties (obj) {
        obj = obj || {};
        let properties = Object.assign(obj, this.defaults()),
            keys = Object.keys(properties),
            length = keys.length;

        while (length--) {

            let property = keys[length];
            if (properties.hasOwnProperty(property)) {

                this[property] = properties[property];
            }
        }
    }
    defaults () {
        return {
            className : '',
            events : {},
            tagName : 'div',
            el : void 0
        };
    }
    _createElement () {
        let tagName = this.tagName || 'div';
        let children = [];
        let className = this.className || '';
        if (this.el && this.el.childNodes) {
            let childNodesArr = this.el.childNodes;
            for (let i = 0, length = childNodesArr.length; i < length; i++) {
                children.push(childNodesArr[i]);
            }
        }
        if(this.el && this.el.parentNode) {
            let newEl = document.createElement(tagName);
            this.el.parentNode.replaceChild(newEl, this.el);
            this.el = newEl;
        } else {

            this.el = document.createElement(tagName);
        }
        if( children.length ) {
            let length = children.length,
                i = 0;
            while (i++ < length) {

                this.el.appendChild(children[i - 1]);
            }
        }
        this.className = className;

    }
    appendNodesToThisEl (nodesArr) {
        nodesArr.forEach( (node) => {

            this.el.appendChild(node);
        });
    }
    cleanAppendNodesToThisEl (nodesArr) {
        this.cleanNode(this.el);
        this.appendNodesToThisEl(nodesArr);
    }
    cleanNode (node) {
        while (node.firstChild) {

            node.removeChild(node.firstChild);
        }
    }
    parseHtmlString (html, type) {
        type = type || 'text/html'; //can also be application/xml and image/svg+xml
        let parser = new DOMParser(), /*is supported by all modern browsers*/
            parsed = parser.parseFromString(html, type);
        return parsed.body.childNodes;
    }
    /**
     * Binds all events to this.el
     * */
    bindEvents (events) { //binds all events to this.el
        events = events || this.events || {};
        let eventsArr = Object.keys(events);
        for (let i = 0, length = eventsArr.length; i < length; i++) {
            let parsedEvent = this._parseEvents(eventsArr[i]);
            this._attachEvent(this.el, parsedEvent.event, parsedEvent.selector, events[eventsArr[i]]);
        }
    }
    /**
     * string format '%eventName% %selector%';
     * Where %selector% is everything after the first space
     * */
    _parseEvents (event) {

        let temp = event.split(eventSplitter);
        return {
            event : temp[0],
            selector : temp[1]
        };
    }
    /**
     * attach an event to the core element
     * and delegates everything
     * stops event out of bubbling over the main element
     * */
    _attachEvent (element, event, delegateSelector, callbackName) {

        element.addEventListener(event, (e) => {
            let target = e.target;

            if (target === element && !delegateSelector) {
                this[callbackName](e, target);
                return;
            }
            while (target && target.parentNode && target !== element.parentNode) {

                if (
                    (target.matches(delegateSelector) && delegateSelector) ||
                    (target === element && !delegateSelector)
                ) {

                    this[callbackName](e, target);
                    target = null;
                } else {

                    target = target.parentNode;
                }
            }
        });
    }
    /**
     * Removes the view from DOM and all its listeners
     * */
    remove () {
        this.stopListening();
        this.off();
        const parent = this.el.parentNode;
        if (parent) {
            this.el.parentNode.removeChild(this.el);
        }
    }
}