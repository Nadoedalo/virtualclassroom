'use strict';
import { Abstract } from '~/core';
export class Model extends Abstract {
    constructor (data, options) {
        super();
        data = Object.assign(this.defaults(), data);
        options = options || {};
        this.collection = options.collection || null;
        this._internal = {}; //Used for getters / setters and storing original values. Do not access directly
        this._pending = false; //used to delay events
        this._silent = false; //used to silence events
        this._xhr = null; //current request. NOTE: abort before calling fetch again if needed
        this.url = '';
        this.attributes = new Proxy({}, { //TODO change attributes type to Map/WeakMap instead of Object? More flexibility.
            set: this._set.bind(this) //this should always reference current instance
        });
        this.set(data);
        return this.initialize(options);
    }
    initialize () {
        return this;
    }
    defaults () {
        return {};
    }
    get id () {
        return this._internal.id || null;
    }
    set id (id) {
        this._internal.id = id;
        return this._internal.id;
    }
    get (name) {
        return this.attributes[name];
    }
    /**
     * proxy call set (target, name, value, receiver)
     * call set on this (prop, value, [options])
     * call set on this alternative ({prop: value}, [{options}])
     * TODO deep proxy?
    * */
    set (key, value, options) {
        let attrs;
        if (typeof key === 'object') {
            attrs = key;
            options = value;
        } else {
            (attrs = {})[key] = value;
        }
        options = options || {};
        this._silent = options.silent || false;
        this._pending = true;
        let changedFlag = false;
        for (let attr in attrs) {
            if (attrs.hasOwnProperty(attr) && attrs[attr] !== this.get(attr)) {
                changedFlag = true;
                this.attributes[attr] = attrs[attr]; //FIXME prettier solution?
            }
        }

        if  (!this._silent && changedFlag) {

            this.trigger('change', this, options);
        }
        this._pending = false;
        this._silent = false;
        return this.attributes; //Backbone returns this, probably for chaining. I think returning representation of the current model attributes will fit better
    }
    _set (target, name, value) {
        //FIXME support for deep proxy!!! Objects, Arrays, Map, Set, WeakMap. Killer-feature.
        //TODO check if value were actually changed
        target[name] = value;

        if (!this._silent) {
            let event = 'change:'+name;

            this.trigger(event, this, {}, value);

            if (!this._pending) {
                this.trigger('change', this, {});
            }
        }
        return target;
    }
    fetch () {
        //TODO support options

        return new Promise((resolve, reject) => {
            this._xhr = new XMLHttpRequest();
            this._xhr.open('GET', this.url);
            this._xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            this._xhr.responseType = 'json';
            this._xhr.onload = () => {
                this.set(this._xhr.response);
                resolve(this._xhr.response);
            };
            this._xhr.onerror = () => {
                reject({
                    error : this._xhr.status,
                    message : this._xhr.response
                });
            };
            this._xhr.send();
        });
    }
}