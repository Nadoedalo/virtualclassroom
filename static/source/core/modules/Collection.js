'use strict';
import { Abstract, Model } from '~/core';
export class Collection extends Abstract {
    constructor (models, options) {
        super();
        options = options || {};
        /**
         * Set vs Array is debatable
         * Sets are quite clumsy when it comes to slicing, sorting and all that kind of stuff
         * But it seems to perform a lot faster
         * Maybe the best way is to de-initialize the Set to array
         * then sort / slice stuff with an array
         * and re-initialize the set from this array
         */
        this.models = [];
        this.Model = options.model || Model; //naming convention - Big first letter means class, small means instance of the class. Though backbone uses small for collection options
        this.add(models);
        return this.initialize(models, options);
    }
    initialize () {
        return this;
    }
    add (data, options) {
        if (!data || typeof data !== 'object') {
            throw new Error('Model or Models array should be provided');
        }
        let singular;
        if (!Array.isArray(data)) {
            data = [data];
            singular = true;
        }
        options = options || {};
        let res = [];
        for (let i = 0, length = data.length; i < length; i++) {
            let model;
            /**
             * Should we allow all instances of Model be added? like (data instanceof Model)?
             * It WILL include all models that extend the core Model class
             * AND it already includes those extended from class attached to this collection
             */
            if (data[i] instanceof this.Model) {
                model = data[i];
            } else {
                //TODO better check data. It should be only an object.
                model = new this.Model(data[i], {
                    collection : this
                });
            }
            //TODO maybe we should merge instead of just simply adding and overriding previous values? If the model exists for example
            model.collection = this;
            if(!model.id) {
                model.id = this.nextId;
            }
            const addMethod = this.addMethod();
            addMethod.call(this.models, model);
            addMethod.call(res, model);
            this.listenTo(model, 'all', this.eventsHoisting); //is this necessary? Just proxies all of the models events onto a collection

            if (!options.silent) {
                this.trigger('add', model, this, options);
            }
        }
        this.trigger('update', this, options);
        return singular ? res[0] : res;
    }
    addMethod () {
        return Array.prototype.push;
    }
    remove (models, options) {
        if (!models || typeof models !== 'object') {
            throw new Error('Model or Models array should be provided');
        }
        if (!Array.isArray(models)) {
            models = [models];
        }
        options = options || {};

        for (let i = 0, length = models.length; i < length; i++) {
            let model = models[i];
            let index = this.indexOf(model.id);
            if (index >= 0) {

                model = this.models.splice(index, 1)[0];
                if(!options.silent) {
                    model.trigger('remove', model, this, options);
                    this.trigger('remove', model, this, options);
                }
                /*fixme we should stop listening previously. Refactor this.stopListening - it seems like it removes ALL listeners*/
                this.stopListening(model);
            }
        }
        if(!options.silent) {
            this.trigger('update', this, options);
        }
        return true;
    }
    reset (models, options) {
        models = models || [];
        options = options || {};
        this.models = [];
        this.add(models, Object.assign(options, {silent: true})); //should we pass options here as well?

        if (!options.silent) {
            this.trigger('reset', this, options);
        }

    }
    placeBefore (beforeId, modelId) {
        let models = this.getModelsArray();
        let modelIndex = this.indexOf(modelId, models);
        let movedModel = models.splice(modelIndex, 1)[0];
        let beforeIndex = this.indexOf(beforeId, models);
        models.splice(beforeIndex, 0, movedModel);
        this.models = models;
        this.trigger('sort', this, models);
        return true;
    }
    placeAfter (afterId, modelId) {
        let models = this.getModelsArray();
        let modelIndex = this.indexOf(modelId, models);
        let movedModel = models.splice(modelIndex, 1)[0];
        let beforeIndex = this.indexOf(afterId, models);
        models.splice(beforeIndex + 1, 0, movedModel);
        this.models = models;
        this.trigger('sort', this, models);
        return true;
    }
    eventsHoisting (...args) {
        const event = args.splice(-1);
        this.trigger(event, ...args);
    }
    indexOf (id, arr) {
        if(typeof id !== 'number') {
            id = +id;
        }
        arr = arr || this.getModelsArray();

        let res = -1;
        for(let i = 0, length = arr.length; i < length; i++) {
            if (arr[i].id === id) {
                res = i;
                break;
            }
        }
        return res;
    }
    at (index) {
        const arr = this.getModelsArray(); //I don't like constant conversions from and to set. Maybe we should switch to just arrays since they fit better.
        return arr[index];
    }
    /**
     * better ID generation? Unique ID for everything?
     * */
    get nextId () {
        this._idCounter = this._idCounter || 0;
        return ++this._idCounter;
    }
    getModelsArray () {
        return this.models;
    }
}