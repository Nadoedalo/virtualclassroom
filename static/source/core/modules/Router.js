'use strict';
import { Abstract } from '~/core';
export class Router extends Abstract {
    
    constructor (options) {
        super();
        options = options || {};
        this.routes = {};
        this._regExpRoutes = {};
        this.initialize(options);
        this._regExpRoutes = this._parseRegExpRoutes(this.routes);
        this.navigate(window.location.pathname);
        return this;
    }
    
    initialize () {
        //TODO call proper URL on page load
        return this;
    }
    /**
     * Just a simple navigate function
     * A former glory of the similar Backbone function
     * */
    navigate (route, options) {
        options = options || {};
        const parsedRoute = this._findRoute(route);
        if (parsedRoute) {
            this.pushHistory(parsedRoute.args, this.routes[parsedRoute.route], route);
            if (!options.silent) {
                this[this.routes[parsedRoute.route]](...parsedRoute.args);
                this.trigger('route', route);
            }

            return true;
        } else {

            console.warn('route "'+route+'" does not exist, falling to default');

            if (this.routes.default) {
                if (!options.silent) {
                    this[this.routes.default](); //TODO parse default? Set default as last defined?
                    this.trigger('route', this.routes.default);
                }
                this.pushHistory({}, this.routes.default, '/'); //default url?
                return true;
            } else {

                throw new Error('Route: "'+route+'" does not exist. Default is undefined');
            }
        }
    }
    pushHistory (data, route, url) {
        this.trigger('route', route, url, data);
        window.history.pushState(data, route, url);
    }
    
    replaceNodes (nodes, node) {
        for (let i = 0, length = nodes.length; i < length; i++) {
            nodes[i].parentNode.replaceChild(node, nodes[i]);
        }
    }
    
    cleanAppend (node, child) {
        this.cleanNode(node);
        node.appendChild(child);
    }

    cleanNode (node) {
        while (node.firstChild) {

            node.removeChild(node.firstChild);
        }
    }
    _parseRegExpRoutes (routes) {
        //TODO optional param ()
        //TODO * all selector
        //FIXME /room/:id will match URL /room/123/3 but it shouldn't
        const regExpReplacer = /:.*/;
        let res = {};
        for(let key in routes){
            let regExpString = key.split('/')
                .map((item) => {
                    return item.replace(regExpReplacer, '(.*)+');
                })
                .join('\\/');
            res['^'+regExpString+'$'] = key;
        }
        return res;
    }
    _findRoute (route) {
        let res = false;
        for (let regExpString in this._regExpRoutes) {
            if (this._regExpRoutes.hasOwnProperty(regExpString)) {

                let regExp = new RegExp(regExpString); //FIXME optimize, can be cached
                if (regExp.test(route)) {
                    let args = route.match(regExp);
                    res = {
                        route : this._regExpRoutes[regExpString],
                        args : args
                    }
                }

            }
        }
        return res;
    }
}