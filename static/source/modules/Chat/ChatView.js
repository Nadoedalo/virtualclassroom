import { View } from '~/core';
import template from './ChatView.html';
export class ChatView extends View {
	initialize() {
		this.importTemplate = template;
		this.className = 'chatView';
		this.render();
		//TODO add actions - edit || delete(only own) && react / response etc
	}
}