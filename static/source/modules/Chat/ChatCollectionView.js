import { View } from '~/core';
import template from './ChatCollectionView.html'
import { ChatView } from './ChatView';

export class ChatCollectionView extends View {
	initialize() {
		this.className = 'chatCollectionView';
		this.importTemplate = template;
		this.render();
		//TODO add send message on submit
		//TODO send typing event
	}
}