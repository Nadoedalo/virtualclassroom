export { ChatCollection as Collection } from './ChatCollection';
export { ChatCollectionView as CollectionView } from './ChatCollectionView';
export { ChatModel as Model } from './ChatModel';
export { ChatView as View } from './ChatView';