import { View } from '~/core';
import template from './ConnectView.html';
export class ConnectView extends View {
	initialize() {
		this.importTemplate = template;
		this.className = 'connectView';
		this.render();
		this.model.on('change', this.render, this);
		return this;
	}
	render () {
		this.template('connectView', this.model.attributes, false).then((html) => {
			this.cleanAppendNodesToThisEl(this.parseHtmlString(html));
		}, console.error.bind(console));
	}
}