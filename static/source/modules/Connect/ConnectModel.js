import {Model} from '~/core';

export class ConnectModel extends Model {
	initialize() {
		this.url = '/api/room';
		this.attributes.loading = true;
		this.fetch().finally(() => {
			this.attributes.loading = false;
		});
		window.test = this;
		return this;
	}
	defaults() {
		return {
			loading: false,
			key: null
		}
	}
}