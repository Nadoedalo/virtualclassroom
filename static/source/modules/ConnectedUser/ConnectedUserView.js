import { View } from '~/core';
import template from './ConnectedUserView.html';
export class ConnectedUserView extends View {
	initialize() {
		this.importTemplate = template;
		this.className = 'connectedUserView';
		//TODO show the ID / name
		//TODO show user voice active / typing in the chat
		//TODO show kick(only master) / mute button
	}
}