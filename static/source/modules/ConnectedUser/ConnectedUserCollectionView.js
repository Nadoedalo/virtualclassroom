import { View } from '~/core';
import template from './ConnectedUserCollectionView.html'
import { ConnectedUserView } from './ConnectedUserView';

export class ConnectedUserCollectionView extends View {
	initialize() {

		this.importTemplate = template;
		this.className = 'connectedUserCollectionView';
		this.render().then(() => {
			this.collection.on('add', this.addOne, this);
			this.collection.on('reset', this.render, this);
		});
	}
	render () {

		return this.template('connectedUserCollectionView').then((html) => {
			this.cleanAppendNodesToThisEl(this.parseHtmlString(html));
			this.collection.models.forEach(this.addOne, this);
		}, console.error.bind(console));
	}
	addOne(model) {

		let view = new ConnectedUserView({model : model});
		this.el.querySelector('.connectedUserList').appendChild(view.el);
	}
}