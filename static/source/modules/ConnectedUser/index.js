export { ConnectedUserCollection as Collection } from './ConnectedUserCollection';
export { ConnectedUserCollectionView as CollectionView } from './ConnectedUserCollectionView';
export { ConnectedUserModel as Model } from './ConnectedUserModel';
export { ConnectedUserView as View } from './ConnectedUserView';