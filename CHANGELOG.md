# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [0.0.2] - 2020-09-06

### Added

* Core front-end infrastructure
    * Abstract class with event model
    * View
    * Model
    * Collection
    * Router
* Module drafts
    * Chat
    * Connected user list
    * User model
* Overall basic architecture
* Updated utility for IDE to resolve paths same way as webpack
* Added TODOs all over the place

## [0.0.1] - 2020-08-29

### Added

* Basic project structure:
    * NodeJS web server under server/ capable of serving static over https://
    * Front-end located under static/
    * Basic tests setup with the help of Jest
    * other minor stuff
* Webpack support
* README.md file