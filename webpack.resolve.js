const path = require('path');
module.exports = {
	resolve: {
		alias : {
			'~' : path.resolve('static/source'),
			'@' : path.resolve('static/design')
		}
	}
};