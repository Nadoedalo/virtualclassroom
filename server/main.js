const server = require('./index'),
			mainConfig = require('./config/main.js');

server.listen(mainConfig.port, function(){
	console.log('Web server live on ' + mainConfig.port);
});