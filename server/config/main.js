module.exports = {
	/*web server configuration*/
	port : 443,
	staticPath : '../build/', //switch to actual FE folder

	/* webpack configuration
	* TODO move to other config?
	* */
	publicPath : {
		dev : '/',
		prod : '/'
	},
	buildDirectory : {
		dev : './build',
		prod : './build'
	},
	entry : './static/source/main.js',
	filename : 'webrtcdemo.bundle.js',
	libraryTarget: 'window', //TODO with webpack 5 should be moved to module for importing
	library: 'WebRTCDemo'
};