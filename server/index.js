//TODO add devserver support with hot reload for both back- and front- end
//TODO add deamon support so it will run in backgroun
const fs = require('fs'),
		express = require('express'),
		app = express(),
		https = require('https'),
		server = https.createServer({
			cert: fs.readFileSync('./server.cert'),
			key: fs.readFileSync('./server.key')
		}, app),
		bodyParser = require('body-parser'),
		session = require('express-session'),
		io = require('socket.io')(server),
		mainDB = require('./db/index.js'); /*TODO use an actual DB? Like MongoDB*/
		mainConfig = require('./config/main.js');
app.use(express.static(mainConfig.staticPath));
app.use(session({
	secret: 'userID',
	resave: false,
	saveUninitialized: true
}));
app.use(bodyParser.json());
require('./routes')(app, mainDB, io);
app.use(function (req, response) { //redirect all other requests to index.html
	fs.readFile(mainConfig.staticPath+'index.html', function(err, data){
		if(err){
			console.log(err);
			response.writeHead(500, { "Content-Type": "text/plain" });
			response.end('No index.html file at');
		}else{
			response.writeHead(200, { "Content-Type": "text/html" });
			response.end(data);
		}
	});
});

module.exports = server;