module.exports = function(app, db, io) {
	const includedAPIs = [ //folder parsing can be added
		'../api/Room'
		//TODO add socket API
		//TODO add webRTC channel
		//TODO add user interface
	];
	const length = includedAPIs.length;
	for(let i = 0; i < length; i++){ //initializing APIs
		require(includedAPIs[i])(app, db, io);
	}
};