const { v4: uuidv4 } = require('uuid');
module.exports = {
	generateID (session) {
		const roomID = uuidv4();
		if(!session.masterRooms){
			session.masterRooms = [];
		}
		session.masterRooms.push(roomID);
		return {
			key : roomID
		};
	},
	checkForMaster (id, session) {
		return session.masterRooms ? session.masterRooms[id] || false : false;
	}
};