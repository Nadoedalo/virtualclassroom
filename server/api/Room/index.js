const room = require('./Room');
function writeResponse(response, res){
	if(res){
		response.json(res);
	}else{
		response.writeHead(502, {'Content-Type': 'text/plain'});
		response.write(JSON.stringify('Something went wrong'));
	}
	response.end();
}
function writeAccessResponse(response, res){
	if(res){
		response.writeHead(204);
	}else{
		response.writeHead(403);
	}
	response.end();
}
module.exports = function(app, db){
	app.get('/api/room', function(request, response){
		const res = room.generateID(request.session);
		writeResponse(response, res);
	});
	app.get('/api/room/:id', function(request, response){
		const id = request.params.id;
		const res = room.checkForMaster(id, request.session);
		writeAccessResponse(response, res);
	});
};