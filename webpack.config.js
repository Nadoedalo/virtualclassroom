'use strict';
//TODO refactor. Split rules to different files - webpack config grows and becomes ugly
//TODO add hot reload
//TODO check on performance of development mode

const path = require('path');
const webpack = require('webpack');
const mainConfig = require('./server/config/main.js');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const resolve = require('./webpack.resolve.js');



const exportFunction = (env, argv) => {
    const devFlag = argv.mode === 'development';
    const publicPath = devFlag ? mainConfig.publicPath.dev : mainConfig.publicPath.prod;
    const buildDirectory = devFlag ? mainConfig.buildDirectory.dev : mainConfig.buildDirectory.prod;
    const mode = devFlag ? 'development' : 'production';
    const devtool = 'inline-source-map';
    const webpack_rules = [];

    const babelLoader = {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env']
            }
        }
    };
    const fontLoader = {
        test: /\.(woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
        loader: 'url-loader?limit=100000'
    };
    const imageLoader =  {
        test: /\.(png)$/,
        loader: 'url-loader?mimetype=image/png&limit=10000'
    };
    const stylesLoader = {
        test: /\.s[ac]ss$/i,
        use: [
            // Creates `style` nodes from JS strings
            'style-loader',
            // Translates CSS into CommonJS
            'css-loader',
            // Compiles Sass to CSS
            'sass-loader'

        ],
    };
    const htmlLoader = {
        test: /\.html$/,
        loader: 'html-loader'
    };
    webpack_rules.push(babelLoader);
    webpack_rules.push(fontLoader);
    webpack_rules.push(imageLoader);
    webpack_rules.push(stylesLoader);
    webpack_rules.push(htmlLoader);

    const plugins = [];
    const cleanWebpackPlugin = new CleanWebpackPlugin({ cleanStaleWebpackAssets: false });
    const webpackOptimizedBundles = new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
    });
    const htmlPlugin = new HtmlWebpackPlugin({
        hash: true,
        filename: 'index.html',
        template: './static/index.html'
    });

    plugins.push(cleanWebpackPlugin);
    if(!devFlag) {
        plugins.push(webpackOptimizedBundles);
    }
    plugins.push(htmlPlugin);

    const webpackOption = {
        entry: mainConfig.entry,
        output: {
            path: path.resolve(__dirname, buildDirectory),
            filename: mainConfig.filename,
            libraryTarget: mainConfig.libraryTarget,
            library: mainConfig.library,
            publicPath: publicPath
        },
        mode: mode,
        devtool: devtool,
        module: {
            rules: webpack_rules
        },
        ...resolve,
        plugins: plugins
    };
    return webpackOption;
};

module.exports = exportFunction;